package test.org.apache.commons.lang3;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Copyright &copy; 2017 - 2017 Well All rights reserved.
 * Author:Well
 * Date:2017-06-29
 * Description:字符串利用测试类
 */

public class StringUtilsTest
{
    private Logger logger=Logger.getLogger(StringUtilsTest.class);

    @Test
    public void testIsEmpty()
    {
        String s=" ";
        boolean result= StringUtils.isEmpty(s);
        logger.info(result);
    }

    @Test
    public void testIsBlank()
    {
        String s=" ";
        boolean result=StringUtils.isBlank(s);
        logger.info(result);
    }
}
