package test.com.well.kernel.util.httpclient;

import com.well.kernel.util.httpclient.HttpAsyncClientUtil;
import com.well.kernel.util.httpclient.HttpResponseFutureCallback;
import org.apache.http.HttpResponse;
import org.junit.Test;

/**
 * Copyright &copy; 2017 - 2017 JeeWell All rights reserved.
 * Author:Well
 * Date:2017-07-01
 * Description:httpclient异步利用类的测试类
 */

public class HttpAsyncClientUtilTest
{
    @Test
    public void testGet() throws Exception
    {
        String uri1="http://ip.taobao.com/service/getIpInfo.php?ip=36.149.67.112";
        String uri2="http://www.baidu.com";
        HttpAsyncClientUtil.get(uri1,new HttpResponseFutureCallback(){
            @Override
            public void completed(HttpResponse response) {
                System.out.println("uri1");
            }
        });
        HttpAsyncClientUtil.get(uri2,new HttpResponseFutureCallback(){
            @Override
            public void completed(HttpResponse response) {
                System.out.println("uri2");
            }
        });
        System.out.println("处理其它业务！");

        Thread.sleep(5000);
    }
}
