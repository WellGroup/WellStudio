package test.com.well.kernel.util.httpclient;

import com.well.kernel.util.httpclient.HttpClientUtil;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Copyright &copy; 2017 - 2017 Well All rights reserved.
 * Author:lenovo
 * Date:2017-06-19
 * Description:http客户端利用测试类
 */
public class HttpClientUtilTest
{
    private Logger logger=Logger.getLogger(HttpClientUtilTest.class);

    @Test
    public void testGet()
    {
//        String uri="http://ip.taobao.com/service/getIpInfo.php?ip=10.1.3.101";
        String uri="http://ip.taobao.com/service/getIpInfo.php?ip=106.14.9.22";
        String content= HttpClientUtil.get(uri);
        content= StringEscapeUtils.unescapeJava(content);
        logger.info(content);
    }
}
