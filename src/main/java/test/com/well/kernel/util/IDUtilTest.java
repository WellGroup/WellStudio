package test.com.well.kernel.util;

import com.well.kernel.util.IDUtil;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Copyright &copy; 2017 - 2017 Well All rights reserved.
 * Author:Well
 * Date:2017-06-19
 * Description:
 */
public class IDUtilTest
{
    private Logger logger=Logger.getLogger(IDUtilTest.class);

    @Test
    public void testGenerate()
    {
        String id= IDUtil.generate();
        logger.info(id);
    }

}
