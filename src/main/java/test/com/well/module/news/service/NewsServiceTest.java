package test.com.well.module.news.service;

import com.well.kernel.util.DateUtil;
import com.well.kernel.util.SpringUtil;
import com.well.module.news.entity.News;
import com.well.module.news.service.NewsService;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

/**
 * Copyright &copy; 2017 - 2017 Well All rights reserved.
 * Author:lenovo
 * Date:2017-06-21
 * Description:新闻测试类
 */


public class NewsServiceTest
{
    private Logger logger=Logger.getLogger(NewsServiceTest.class);

    @Test
    public void testInsertITNews() throws IOException
    {
        NewsService newsService=SpringUtil.getBean(NewsService.class);
        int result=newsService.insertITNews();
        if(result>0){
            logger.info(DateUtil.getDateTime()+" 抓取【"+result+"】个IT新闻成功！");
        }
        else if(result==0){
            logger.info(DateUtil.getDateTime()+" 没有抓取到新的IT新闻");
        }
        else{
            logger.error(DateUtil.getDateTime()+" 抓取IT新闻失败！");
        }
    }

    @Test
    public void testInsertMovieNews() throws IOException
    {
        NewsService newsService=SpringUtil.getBean(NewsService.class);
        int result=newsService.insertMovieNews();
        if(result>0){
            logger.info(DateUtil.getDateTime()+" 抓取【"+result+"】个Movie新闻成功！");
        }
        else if(result==0){
            logger.info(DateUtil.getDateTime()+" 没有抓取到新的Movie新闻");
        }
        else{
            logger.error(DateUtil.getDateTime()+" 抓取Movie新闻失败！");
        }
    }

    @Test
    public void testInsertNBANews() throws IOException
    {
        NewsService newsService=SpringUtil.getBean(NewsService.class);
        int result=newsService.insertNBANews();
        if(result>0){
            logger.info(DateUtil.getDateTime()+" 抓取【"+result+"】个NBA新闻成功！");
        }
        else if(result==0){
            logger.info(DateUtil.getDateTime()+" 没有抓取到新的NBA新闻");
        }
        else{
            logger.error(DateUtil.getDateTime()+" 抓取NBA新闻失败！");
        }
    }

    @Test
    public void testGetLatestNewsList(){
        NewsService newsService=SpringUtil.getBean(NewsService.class);
        List<News> newsList=newsService.getLatestNewsList(1,10);
        logger.info(newsList.size());
    }

}
