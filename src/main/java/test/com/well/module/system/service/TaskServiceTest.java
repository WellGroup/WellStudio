package test.com.well.module.system.service;

import com.well.kernel.util.SpringUtil;
import com.well.module.system.service.TaskService;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Copyright &copy; 2017 - 2017 Well All rights reserved.
 * Author:Well
 * Date:2017-06-22;
 * Description:定时器测试类
 */
public class TaskServiceTest
{
    private Logger logger=Logger.getLogger(TaskServiceTest.class);

    @Test
    public void testInsertNews()
    {
        TaskService taskService= SpringUtil.getBean(TaskService.class);
        taskService.insertNews();
    }
}
