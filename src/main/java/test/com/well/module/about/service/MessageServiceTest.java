package test.com.well.module.about.service;

import com.well.kernel.util.SpringUtil;
import com.well.module.about.entity.Message;
import com.well.module.about.service.MessageService;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Copyright &copy; 2017 - 2017 Well All rights reserved.
 * Author:Well
 * Date:2017-06-29
 * Description:留言service测试类
 */

public class MessageServiceTest
{
    private Logger logger=Logger.getLogger(MessageServiceTest.class);

    private MessageService messageService= SpringUtil.getBean(MessageService.class);

    @Test
    public void testInsert()
    {
        Message message=new Message();
        message.setName("王先生");
        message.setEmail("18260012083@163.com");
        message.setMobile("18260012083");
        message.setContent("说点什么吧");
        message.setIp("127.0.0.1");
        int result=messageService.insert(message);
        if(result>0)
        {
            logger.info("留言成功！");
        }
        else
        {
            logger.info("留言失败！");
        }
    }

}
