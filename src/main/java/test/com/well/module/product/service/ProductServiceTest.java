package test.com.well.module.product.service;

import com.well.kernel.util.SpringUtil;
import com.well.module.product.entity.Product;
import com.well.module.product.service.ProductService;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright &copy; 2017 - 2017 Well All rights reserved.
 * Author:Well
 * Date:2017-06-28
 * Description:产品service测试
 */

public class ProductServiceTest
{
    private Logger logger=Logger.getLogger(ProductServiceTest.class);

    private ProductService productService= SpringUtil.getBean(ProductService.class);

    @Test
    public void testgetProductArray()
    {
        productService.getProductArray();
    }

    @Test
    public void testInsert()
    {
        Product product=new Product();
        product.setName("其它产品");
        product.setSummary("还有很多，敬请期待...");
        product.setThumbnail("static/image/other_product.jpg");
        int result=productService.insert(product);
        logger.info(result);
    }
}
