package com.well.kernel.shiro;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authc.credential.PasswordService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Copyright:Nanjing Xunoi Info Tech Co.,Ltd 2016
 * Author:Well
 * Date:2016/12/27
 * Description:
 *  自定义登录的校验匹配
 * 		新增用户和管理员时，采用PasswordService提供的加密方法加密，
 * 		那用户登录时，就采用PasswordService提供的方法进行密码匹配
 */

public class LoginCredentialsMatcher extends HashedCredentialsMatcher
{
	@Autowired
	private PasswordService passwordService;
	
	/**
	 * 登录时密码的校验匹配
	 * @param token 需要认证token
	 * @param info 数据库中已经存在的info
	 * @return true---校验成功
	 * 			false---校验失败
	 */
	@Override
	public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info)
	{
		UsernamePasswordToken usernamePasswordToken=(UsernamePasswordToken)token;
		
		// 前台输入的密码，通过shiro框架认证时，已经将密码转换成数组
		char[] tokenPassword=usernamePasswordToken.getPassword();
		// 从数据库中获取的用户密码，在用户注册时已经通过passwordService.encryptPassword方法加密过了
		String password=String.valueOf(info.getCredentials());
		
		// 密码的校验匹配
		return passwordService.passwordsMatch(tokenPassword, password);
	}
	
	
}
