package com.well.kernel.mybatis;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.internal.DefaultCommentGenerator;

import java.util.List;

/**
 * Copyright:Nanjing Xunoi Info Tech Co.,Ltd 2016
 * Author:Well
 * Date:2016/12/27
 * Description:
 *  自定义注释生成器
 */

public class CommentGenerator extends DefaultCommentGenerator
{
    /**
     * 增加字段的注释和注解
     * @param field
     * @param introspectedTable
     * @param introspectedColumn
     */
    @Override
    public void addFieldComment(Field field, IntrospectedTable introspectedTable, IntrospectedColumn introspectedColumn) {
        /* 字段注释 */
        field.addJavaDocLine("/* "+introspectedColumn.getRemarks()+" */");
        /* 字段注解 */
        StringBuilder sb = new StringBuilder();
        List<IntrospectedColumn> primaryKeyColumnList = introspectedTable.getPrimaryKeyColumns();
        for(IntrospectedColumn primaryKeyColumn:primaryKeyColumnList)
        {
            if(introspectedColumn == primaryKeyColumn)
            {
                field.addAnnotation("@Id");
                field.addAnnotation("@GeneratedValue(generator = \"UUID\")");
                break;
            }
        }
        sb.append("@Column(");
        sb.append("name=\"").append(introspectedColumn.getActualColumnName()).append("\"");
        sb.append(",columnDefinition=\"").append(introspectedColumn.getRemarks()).append("\"");
        FullyQualifiedJavaType fullyQualifiedJavaType = introspectedColumn.getFullyQualifiedJavaType();
        if(!"Date".equalsIgnoreCase(fullyQualifiedJavaType.getShortName()))
        {
            sb.append(",length=").append(introspectedColumn.getLength());
        }
        sb.append(",nullable=").append(introspectedColumn.isNullable());
        sb.append(")");
        field.addAnnotation(sb.toString());
    }

    /**
     * 增加实体类的引用类型、注释和注解
     * @param topLevelClass
     * @param introspectedTable
     */
    @Override
    public void addModelClassComment(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        FullyQualifiedJavaType fullyQualifiedJavaType=new FullyQualifiedJavaType("javax.persistence.*");
        topLevelClass.addImportedType(fullyQualifiedJavaType);

        StringBuilder sb = new StringBuilder();
        topLevelClass.addJavaDocLine("/**");
        sb.append(" * ");
        sb.append(introspectedTable.getFullyQualifiedTable());
        topLevelClass.addJavaDocLine(sb.toString().replace("\n", " "));
        sb.setLength(0);
        sb.append(" * @author ");
        sb.append(introspectedTable.getRemarks());
        topLevelClass.addJavaDocLine(" */");
        topLevelClass.addAnnotation("@Entity");
        topLevelClass.addAnnotation("@Table(name=\""+introspectedTable.getFullyQualifiedTableNameAtRuntime()+"\")");
    }

    
    
}
