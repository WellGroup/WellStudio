package com.well.kernel.mybatis;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.internal.types.JavaTypeResolverDefaultImpl;

/**
 * Copyright:Nanjing Xunoi Info Tech Co.,Ltd 2016
 * Author:Well
 * Date:2017/1/5
 * Description:
 */

public class JavaTypeResolver extends JavaTypeResolverDefaultImpl
{
    @Override
    protected FullyQualifiedJavaType calculateBigDecimalReplacement(IntrospectedColumn column, FullyQualifiedJavaType defaultType) {
        FullyQualifiedJavaType answer;
        if(column.getScale() <= 0 && column.getLength() <= 18 && !this.forceBigDecimals) {
            if(column.getLength() > 9) {
                answer = new FullyQualifiedJavaType(Long.class.getName());
            }else {
                answer = new FullyQualifiedJavaType(Integer.class.getName());
            }
        } else {
            answer = defaultType;
        }

        return answer;
    }
    
    
}
