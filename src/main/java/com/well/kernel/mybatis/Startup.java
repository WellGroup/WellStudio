package com.well.kernel.mybatis;

import org.apache.log4j.Logger;
import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.GeneratedXmlFile;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.api.dom.xml.Document;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.TableConfiguration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Copyright:Nanjing Xunoi Info Tech Co.,Ltd 2016
 * Author:Well
 * Date:2016/12/27
 * Description:
 *  mybatis代码生成器的启动类
 */

public class Startup
{
    private static Logger logger=Logger.getLogger(Startup.class);

    public static void main(String[] args)
    {
        try
        {
            List<String> warnings = new ArrayList<String>();
            boolean overwrite = true;
            InputStream is=Thread.currentThread().getContextClassLoader().getResourceAsStream("mybatis/mybatis-generator.xml");
            ConfigurationParser cp = new ConfigurationParser(warnings);
            Configuration config=cp.parseConfiguration(is);
            
            Document document=config.toDocument();
            logger.info(document.toString());
            
            TableConfiguration tableConfiguration=new TableConfiguration(config.getContext("mysqlContext"));
            tableConfiguration.setTableName("user");
            
            DefaultShellCallback callback = new DefaultShellCallback(overwrite);
            MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
            myBatisGenerator.generate(null);
            
            List<GeneratedJavaFile> generatedJavaFileList=myBatisGenerator.getGeneratedJavaFiles();
            Iterator<GeneratedJavaFile> iteratorJava=generatedJavaFileList.iterator();
            GeneratedJavaFile generatedJavaFile=null;
            while(iteratorJava.hasNext())
            {
            	generatedJavaFile=iteratorJava.next();
            	logger.info(generatedJavaFile.getFileName());
            }
            
            List<GeneratedXmlFile> generatedXmlFileList=myBatisGenerator.getGeneratedXmlFiles();
            Iterator<GeneratedXmlFile> iteratorXml=generatedXmlFileList.iterator();
            GeneratedXmlFile generatedXmlFile=null;
            while(iteratorXml.hasNext())
            {
            	generatedXmlFile=iteratorXml.next();
            	logger.info(generatedXmlFile.getFileName());
            }
            
            if(warnings.size()==0)
            {
                logger.info("生成 entity mapper xml 成功！");
            }
            else
            {
                logger.warn("警告：");
                for(String warning:warnings)
                {
                    logger.warn(warning);
                }
            }
        }
        catch (Exception e)
        {
            logger.error(e);
        }
    }
}
