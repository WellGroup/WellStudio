package com.well.kernel.mybatis.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Copyright:Nanjing Xunoi Info Tech Co.,Ltd 2016
 * Author:Well
 * Date:2016/12/29
 * Description:实体类的抽象基类
 */

@MappedSuperclass
public abstract class ABaseEntity implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(generator = "UUID")
    @Column(name="id",columnDefinition="主键",length=32,nullable=false)
	private String id;
	
	@Column(name="insert_user",columnDefinition="新增人的id",length=32)
	private String insertUser;
	
	@Column(name="insert_date",columnDefinition="新增时间")
	private Date insertDate;
	
	@Column(name="update_user",columnDefinition="修改人的id",length=50)
	private String updateUser;

	@Column(name="update_date",columnDefinition="修改时间")
	private Date updateDate;
	
	@Column(name="is_use",columnDefinition="是否可用，0-禁用，1-可用",length=1,nullable = false)
	private Integer isUse;
	
	@Column(name="memo",columnDefinition="备注",length=255)
	private String memo;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInsertUser() {
		return insertUser;
	}

	public void setInsertUser(String insertUser) {
		this.insertUser = insertUser;
	}

	public Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Integer getIsUse() {
		return isUse;
	}

	public void setIsUse(Integer isUse) {
		this.isUse = isUse;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}
	
	
	
}
