package com.well.kernel.service;

import com.github.pagehelper.PageInfo;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * Copyright:Nanjing Xunoi Info Tech Co.,Ltd 2016
 * Author:Well
 * Date:2017/05/24
 * Description:
 */

public abstract class AbstractService<T>
{
	@Autowired
    private Mapper<T> mapper;
	
	/**
     * 新增实体
     * @param entity
     * @return
     */
    public int insert(T entity)
    {
        return mapper.insert(entity);
    }

    /**
     * 选择新增实体，推荐使用
     * 只新增非空属性
     * @param entity
     * @return
     */
    public int insertSelective(T entity)
    {
        return mapper.insertSelective(entity);
    }

    /**
     * 根据主键删除实体
     * @param id
     * @return
     */
    public int delete(String id)
    {
    	return mapper.deleteByPrimaryKey(id);
    }
    
    /**
     * 根据实体非空属性的并集删除实体
     * @param entity
     * @return
     */
    public int delete(T entity)
    {
        return mapper.delete(entity);
    }
    
    /**
     * 批量删除所有数据
     * @return
     */
    public int deleteList()
    {
        return mapper.delete(null);
    }

    /**
     * 更新实体
     * @param entity
     * @return
     */
    public int update(T entity)
    {
        return mapper.updateByPrimaryKey(entity);
    }

    /**
     * 选择更新实体，推荐使用
     * 只更新非空属性
     * @param entity
     * @return
     */
    public int updateSelective(T entity)
    {
        return mapper.updateByPrimaryKeySelective(entity);
    }

    /**
     * 查询全部实体列表
     * @return
     */
    public List<T> selectList()
    {
        return mapper.selectAll();
    }

    /**
     * 根据实体非空属性的并集查询实体列表
     * @param entity
     * @return
     */
    public List<T> selectList(T entity)
    {
        return mapper.select(entity);
    }

    /**
     * 根据实体非空属性的并集查询实体
     * 只返回一个对象
     * @param entity
     * @return
     */
    public T select(T entity)
    {
        return mapper.selectOne(entity);
    }

    /**
     * 根据主键查询实体
     * @param id
     * @return
     */
    public T select(String id)
    {
        return mapper.selectByPrimaryKey(id);
    }

    /**
     * 分页查询
     * @param page 页码，从1开始
     * @param rows 一页显示的条数
     * @return
     */
    public PageInfo<T> selectPageInfo(int page,int rows)
    {
        return selectPageInfo(null,page,rows);
    }

    /**
     * 根据实体非空属性的并集分页查询
     * @param entity
     * @param page 页码，从0开始
     * @param rows 一页显示的条数
     * @return
     */
    public PageInfo<T> selectPageInfo(T entity,int page,int rows)
    {
        List<T> list=mapper.selectByRowBounds(entity,new RowBounds(page,rows));
        PageInfo<T> pageInfo=new PageInfo<T>(list);
        return pageInfo;
    }
	
    /**
     * 查询，这个方法有待调整
     * @return
     */
    public long selectCount()
    {
    	mapper.selectCountByExample(null);
    	return mapper.selectCount(null);
    }
    
}
