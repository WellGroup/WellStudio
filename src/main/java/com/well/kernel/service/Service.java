package com.well.kernel.service;

import java.util.List;

import com.github.pagehelper.PageInfo;

/**
 * Copyright:
 * Author:Well
 * Date:2017/06/07
 * Description: 业务逻辑接口
 */

public interface Service<T> 
{
	int insert(T t);
	
	int delete(T t);
	
	int update(T t);
	
	List<T> selectList();
	
	List<T> selectList(T t);
	
	T select(T t);
	
	PageInfo<T> selectPageInfo(int page, int rows);
	
	
	
	long selectCount();
}
