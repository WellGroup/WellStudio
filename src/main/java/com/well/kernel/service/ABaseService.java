package com.well.kernel.service;

import com.github.pagehelper.PageInfo;
import com.well.kernel.mybatis.persistence.ABaseEntity;
import com.well.kernel.util.IDUtil;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.List;

/**
 * Copyright:Nanjing Xunoi Info Tech Co.,Ltd 2016
 * Author:Well
 * Date:2016/12/29
 * Description:
 */

public abstract class ABaseService<T extends ABaseEntity> extends AbstractService<T>
{
    private Logger logger=Logger.getLogger(ABaseService.class);

    @Override
    public int insert(T entity) 
    {
    	Date date=new Date();

        if(StringUtils.isEmpty(entity.getId())){
            String id=IDUtil.generate();
            entity.setId(id);
        }
    	entity.setInsertDate(date);
    	entity.setUpdateDate(date);
        if(null==entity.getIsUse()){
            Integer isUse=1;
            entity.setIsUse(isUse);
        }
    	return super.insert(entity);
    }
    
    @Override
    public int insertSelective(T entity) 
    {
    	String id=IDUtil.generate();
    	Date date=new Date();
    	Integer isUse=1;
    	
    	entity.setId(id);
    	entity.setInsertDate(date);
    	entity.setUpdateDate(date);
    	entity.setIsUse(isUse);
    	return super.insertSelective(entity);
    }
    
    @Override
    public int delete(String id) 
    {
    	if(StringUtils.isEmpty(id))
    	{
    		logger.warn("参数为空！");
    		return 0;
    	}
    	return super.delete(id);
    }
    
    @Override
    public int delete(T entity)
    {
    	return super.delete(entity);
    }
    
    @Override
    public int update(T entity)
    {
    	if(StringUtils.isEmpty(entity.getId()))
    	{
    		logger.warn("主键不能为空！");
    		return 0;
    	}
    	entity.setUpdateDate(new Date());
    	return super.update(entity);
    }
    
    @Override
    public int updateSelective(T entity) 
    {
    	if(StringUtils.isEmpty(entity.getId()))
    	{
    		logger.warn("主键不能为空！");
    		return 0;
    	}
    	entity.setUpdateDate(new Date());
    	return super.updateSelective(entity);
    }
    
    @Override
    public List<T> selectList() 
    {
    	return super.selectList();
    }
    
    @Override
    public List<T> selectList(T entity) 
    {
    	return super.selectList(entity);
    }
    
    @Override
    public T select(T entity) 
    {
    	return super.select(entity);
    }
    
    @Override
    public T select(String id) 
    {
    	if(StringUtils.isEmpty(id))
    	{
    		logger.warn("主键为空！");
    		return null;
    	}
    	return super.select(id);
    }
    
    @Override
    public PageInfo<T> selectPageInfo(int page, int rows)
    {
    	return super.selectPageInfo(page, rows);
    }
    
    @Override
    public PageInfo<T> selectPageInfo(T entity, int page, int rows)
    {
    	return super.selectPageInfo(entity, page, rows);
    }
}
