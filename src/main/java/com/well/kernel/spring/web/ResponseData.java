package com.well.kernel.spring.web;

import java.io.Serializable;

/**
 * Copyright:
 * Author:Well
 * Date:2016-12-12
 * Description:响应数据类，AJAX请求时，在controller类中使用该类返回
 */

public class ResponseData implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private boolean success;
    private String code;
    private String message;
    private Object data;

    public ResponseData()
    {
        this.success=true;
        this.message="操作成功！";
    }

    public ResponseData(boolean success)
    {
        this.success=success;
    }

    public ResponseData(String message)
    {
        this.success=true;
        this.message=message;
    }

    public ResponseData(boolean success, String message)
    {
        this.success=success;
        this.message=message;
    }

    public ResponseData(boolean success,Object data)
    {
        this.success=success;
        this.data=data;
    }

    public ResponseData(String message, Object data)
    {
        this.success=true;
        this.message=message;
        this.data=data;
    }

    public ResponseData(boolean success, String message, Object data)
    {
        this.success=success;
        this.message=message;
        this.data=data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResponseData{" +
                "success=" + success +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}