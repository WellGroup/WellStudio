package com.well.kernel.spring.web.easyui;

import java.io.Serializable;
import java.util.List;

/**
 * Copyright:
 * Author:Well
 * Date:2016-12-12
 * Description:AJAX请求时，响应到Easyui的datagrid的数据
 */

public class Datagrid<T> implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	/* 数据列表记录 */
	private List<T> rows;
	
	/* 查询的列表总数 */
	private Long total;

	public Datagrid(){}
	
	public Datagrid(List<T> rows,Long total)
	{
		this.rows=rows;
		this.total=total;
	}

	public List<T> getRows() {
		return rows;
	}

	public void setRows(List<T> rows) {
		this.rows = rows;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "Datagrid [rows=" + rows + ", total=" + total + "]";
	}
	
}
