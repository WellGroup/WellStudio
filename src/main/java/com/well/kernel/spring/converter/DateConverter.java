package com.well.kernel.spring.converter;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Copyright:Nanjing Xunoi Info Tech Co.,Ltd 2016
 * Author:Well
 * Date:2017/1/10
 * Description:
 * 日期转换器
 */

public class DateConverter implements Converter<String ,Date>
{
    private static Logger logger=Logger.getLogger(DateConverter.class);

    /**
     * 将前台传递的 yyyy-MM-dd 的日期字符换转换为 Date 类型
     * @param s
     * @return
     */
    @Override
    public Date convert(String s) {
        try
        {
            if(StringUtils.isNotBlank(s))
            {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                dateFormat.setLenient(false);
                return dateFormat.parse(s);
            }
        }
        catch (Exception e)
        {
            logger.error(e);
        }
        return null;
    }
}
