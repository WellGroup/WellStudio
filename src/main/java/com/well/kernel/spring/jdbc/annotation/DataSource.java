package com.well.kernel.spring.jdbc.annotation;

import java.lang.annotation.*;

/**
 * Copyright &copy; 2017 - 2017 Well All rights reserved.
 * Author:Well
 * Date:2017-10-25
 * Description:使用数据源的注解
 */

@Inherited
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DataSource {
    boolean slave() default false;
}
