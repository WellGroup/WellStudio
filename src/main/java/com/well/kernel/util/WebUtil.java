package com.well.kernel.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.well.kernel.spring.web.ResponseData;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.List;

/**
 * Copyright &copy; 2017 - 2017 JeeWell All rights reserved.
 * Author:Well
 * Date:2017-02-05
 * Description: Web 的利用类，包括request和response
 */

public class WebUtil {
    private static Logger logger=Logger.getLogger(WebUtil.class);

    private WebUtil(){}

    /**
     * 获取前台请求的字符串值
     * @param request
     * @param name 前台请求的参数名称
     * @return
     */
    public static String getString(ServletRequest request,String name)
    {
        return request.getParameter(name);
    }

    /**
     * 获取前台请求的字符串数组
     * @param request
     * @param name 前台请求的参数名称
     * @return
     */
    public static String[] getStrings(ServletRequest request,String name)
    {
        return request.getParameterValues(name);
    }

    /**
     * 获取前台请求的字符串列表
     * @param request
     * @param name 前台请求的参数名称
     * @return
     */
    public static List<String> getStringList(ServletRequest request,String name)
    {
        List<String> stringList = null;
        String[] strings=getStrings(request, name);
        if(null!=strings)
        {
            stringList= Arrays.asList(strings);
        }
        return stringList;
    }

    /**
     * 判断是否为 ajax 请求
     * @param request
     * @return
     */
    public static boolean isAjax(HttpServletRequest request)
    {
        String header=request.getHeader("x-requested-with");
        if(null!=header && StringUtils.equalsIgnoreCase(header, "XMLHttpRequest"))
        {
            return true;
        }
        return false;
    }

    /**
     * 获取请求的IP地址
     * 经过下面一系列的代理，IP地址会发生改变
     *  X-Forwarded-For,Proxy-Client-IP,WL-Proxy-Client-IP,HTTP_CLIENT_IP,HTTP_X_FORWARDED_FOR
     * @param request
     * @return 前台请求的IP地址
     * 		        出现异常返回null
     */
    public static String getIP(HttpServletRequest request)
    {
        String ip = null;
        try
        {
            String[] proxys = { "X-Forwarded-For", "Proxy-Client-IP", "WL-Proxy-Client-IP", "HTTP_CLIENT_IP", "HTTP_X_FORWARDED_FOR" };
            for (String proxy : proxys)
            {
                ip = request.getHeader(proxy);
                if (StringUtils.isBlank(ip) || StringUtils.equalsIgnoreCase("unkown",ip))
                {
                    continue;
                }
                else
                {
                    break;
                }
            }
            if (StringUtils.isBlank(ip) || StringUtils.equalsIgnoreCase("unkown",ip))
            {
                ip = request.getRemoteAddr();
                if (StringUtils.equals("127.0.0.1",ip) || StringUtils.equals("0:0:0:0:0:0:0:1",ip))
                {
                    InetAddress inetAddress = InetAddress.getLocalHost();
                    ip = inetAddress.getHostAddress();
                }
            }
            if (null != ip && ip.length() > 15)
            {
                int index = ip.indexOf(",");
                if (index > 0)
                {
                    ip = ip.substring(0, index);
                }
            }
        }
        catch (Exception e)
        {
            logger.error(e);
        }
        return ip;
    }



    /**
     * 发送 responseData 对象到前台
     * @param response
     * @param responseData
     */
    public static void renderResponseData(HttpServletResponse response,ResponseData responseData)
    {
        try
        {
            ObjectMapper mapper = new ObjectMapper();
            String json=mapper.writeValueAsString(responseData);
            renderJSON(response,json);
        }
        catch (Exception e)
        {
            logger.error(e);
        }
    }

    /**
     * 发送 json 内容
     * @param response 响应
     * @param content json格式的内容
     */
    public static void renderJSON(HttpServletResponse response, String content)
    {
        render(response, "application/json;charset=UTF-8", content);
    }

    /**
     * 发送指定contentType类型的内容到前台
     * @param response 响应
     * @param contentType 类型
     * @param content 内容
     */
    public static void render(HttpServletResponse response,String contentType,String content)
    {
        PrintWriter writer=null;
        try
        {
            setProperty(response, contentType);
            writer=response.getWriter();
            writer.write(content);
        }
        catch (IOException e)
        {
            logger.error(e);
        }
        finally
        {
            if(null!=writer)
            {
                writer.flush();
                writer.close();
            }
        }
    }

    /**
     * 设置响应的属性
     * @param response
     * @param contentType
     */
    private static void setProperty(HttpServletResponse response,String contentType)
    {
        response.setCharacterEncoding("UTF-8");
        response.setContentType(contentType);
        response.setHeader("Pragma", "No-cache");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
    }

}
