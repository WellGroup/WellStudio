package com.well.kernel.util.httpclient;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.concurrent.FutureCallback;
import org.apache.log4j.Logger;

/**
 * Copyright &copy; 2017 - 2017 JeeWell All rights reserved.
 * Author:Well
 * Date:2017-07-02
 * Description:httpresponse的反光类，使用httpasyncclient访问uri是的回调类
 */

public class HttpResponseFutureCallback implements FutureCallback<HttpResponse>
{
    private static Logger logger=Logger.getLogger(HttpResponseFutureCallback.class);

    public void completed(final HttpResponse response) {

    }

    public void failed(final Exception ex) {
        logger.error(ex);
    }

    public void cancelled() {
        logger.warn("cancelled");
    }

}
