package com.well.kernel.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 * Copyright &copy; 2017 - 2017 JeeWell All rights reserved.
 * Author:Well
 * Date:2017-02-08
 * Description:日期的利用类
 *      日期时间的处理方法可以也可以参考 org.apache.commons.lang3.time.DateUtils和org.apache.commons.lang3.time.DateFormatUtils
 */

public class DateUtil
{
    private static Logger logger= Logger.getLogger(DateUtil.class);

    /* 日期时间的转换格式 */
    public static final String DATE_TIME_PATTERN="yyyy-MM-dd HH:mm:ss";

    /* 日期的转换格式 */
    public static final String DATE_PATTERN="yyyy-MM-dd";

    /* 时间的转换格式 */
    public static final String TIME_PATTERN="HH:mm:ss";

    /* 年份的转换格式 */
    public static final String YEAR_PATTERN="yyyy";

    /* 月份的转换格式 */
    public static final String MONTH_PATTERN="MM";

    /* 天的转换格式 */
    public static final String DAY_PATTERN="dd";

    /* 时的转换格式 */
    public static final String HOUR_PATTERN="HH";

    /* 分的转换格式 */
    public static final String MINUTE_PATTERN="mm";

    /* 秒的转换格式 */
    public static final String SECOND_PATTERN="ss";

    /* 时区，东八区，即北京时间 */
    public static final String TIME_ZONE="GMT+8";

    /**
     * 构造函数私有化
     */
    private DateUtil(){}

    /**
     * 获取当前的日期时间
     * @return
     */
    public static String getDateTime()
    {
        return format(new Date(), DATE_TIME_PATTERN);
    }

    /**
     * 获取当前日期
     * @return
     */
    public static String getDate()
    {
        return format(new Date(), DATE_PATTERN);
    }

    /**
     * 获取当前时间
     * @return
     */
    public static String getTime()
    {
        return format(new Date(), TIME_PATTERN);
    }

    /**
     * 获取当前年份
     * @return
     */
    public static String getYear()
    {
        return format(new Date(),YEAR_PATTERN);
    }

    /**
     * 获取当前月份
     * @return
     */
    public static String getMonth()
    {
        return format(new Date(),MONTH_PATTERN);
    }

    /**
     * 获取当前天
     * @return
     */
    public static String getDay()
    {
        return format(new Date(),DAY_PATTERN);
    }

    /**
     * 获取当前小时
     * @return
     */
    public static String getHour()
    {
        return format(new Date(),HOUR_PATTERN);
    }

    /**
     * 获取当前分钟
     * @return
     */
    public static String getMinute()
    {
        return format(new Date(),MINUTE_PATTERN);
    }

    /**
     * 获取当前秒钟
     * @return
     */
    public static String getSecond()
    {
        return format(new Date(),SECOND_PATTERN);
    }

    /**
     * 将Date类型格式化成指定格式的字符串类型
     * @param date
     * @param format 格式
     * @return
     */
    public static String format(Date date,String format)
    {
        String s=null;
        if(null!=date && StringUtils.isNotBlank(format))
        {
            s= DateFormatUtils.format(date, format);
        }
        else
        {
            logger.error("参数不能为空！");
        }
        return s;
    }

    /**
     * 将字符串类型的日期时间转换为Date类型
     * @param s 字符串的日期时间
     * @param format 格式
     * @return
     * @throws
     */
    public static Date parse(String s,String format)
    {
        Date date=null;
        try
        {
            if(StringUtils.isNoneBlank(s, format))
            {
                date= DateUtils.parseDate(s, format);
            }
            else
            {
                logger.error("参数不能为空！");
            }
        }
        catch (Exception e)
        {
            logger.error(e);
        }
        return date;
    }

    /**
     * 判断年份是否为闰年
     * @param year
     * @return
     */
    public static boolean isLeapYear(String year)
    {
        return isLeapYear(Integer.parseInt(year));
    }

    /**
     * 判断年份是否为闰年
     * @param year 年份
     * @return
     */
    public static boolean isLeapYear(int year)
    {
        boolean leapYear=((year%400)==0);
        if(!leapYear)
        {
            leapYear=((year%4)==0) && ((year%100)!=0);
        }
        return leapYear;
    }

}
