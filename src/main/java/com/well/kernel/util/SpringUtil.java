package com.well.kernel.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 
 * @author Well
 * 描述:spring的util类
 * 		编写测试用例时，获取注入在spring中的bean
 */

public class SpringUtil 
{
	/* spring配置文件 */
	private static final String SPRING_CONFIG = "spring/spring.xml";
	
	/* 获取spring的上下文 */
	private static ApplicationContext applicationContext = new ClassPathXmlApplicationContext(SPRING_CONFIG);

	/**

	 * 获取对应类型的bean

	 * @param clazz 类型

	 * @return bean

	 */
	public static <T> T getBean(Class<T> clazz)
	{
		return applicationContext.getBean(clazz);
	}

	/**

	 * 获取spring中指定bean名称的bean

	 * @param name bean名称

	 * @return bean

	 */
	public static Object getBean(String name)
	{
		return applicationContext.getBean(name);
	}
}
