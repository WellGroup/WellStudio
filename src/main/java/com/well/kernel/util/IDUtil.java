package com.well.kernel.util;

import java.util.UUID;

import org.apache.log4j.Logger;

/**
 * Copyright:
 * Author:Well
 * Date:2017/1/10
 * Description:
 *  主键 util 类
 */

public class IDUtil
{
    private static Logger logger=Logger.getLogger(IDUtil.class);

    private IDUtil(){}

    /**
     * 生成 32位 uuid
     * @return
     */
    public static String generate()
    {
        return UUID.randomUUID().toString().replace("-","");
    }

    /**
     * 主键id的长度
     * @return
     */
    public static int length()
    {
        return generate().length();
    }

}
