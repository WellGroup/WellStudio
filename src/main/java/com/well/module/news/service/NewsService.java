package com.well.module.news.service;

import com.github.pagehelper.PageInfo;
import com.well.kernel.service.ABaseService;
import com.well.kernel.spring.jdbc.annotation.DataSource;
import com.well.module.news.entity.News;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Copyright &copy; 2017 - 2017 Well All rights reserved.
 * Author:Well
 * Date:2017-06-21
 * Description:新闻的业务类
 */

@Service
public class NewsService extends ABaseService<News>
{
    private static Logger logger=Logger.getLogger(NewsService.class);

    /**
     * 获取最新的新闻列表
     * @param type 新闻类型
     * @param rows 新闻条数
     * @return
     */
    public List<News> getLatestNewsList(int type,int rows)
    {
        News news=new News();
        news.setType(type);
        PageInfo<News> pageInfo=super.selectPageInfo(news,1,rows);
        return pageInfo.getList();
    }

    /**
     * 根据新闻类型获取新闻列表，分页
     * @param type 新闻类型
     * @param pageNum 当前页
     * @param pageSize 每一页的数量
     * @return
     */
    public PageInfo<News> selectNewsListByType(int type,int pageNum,int pageSize)
    {
        News news=new News();
        news.setType(type);
        return super.selectPageInfo(news,pageNum,pageSize);
    }

    /**
     *
     * @return
     */
    @Transactional
    public int insertITNews()
    {
        int result=0;
        try
        {
            List<News> newsList=grabITNews();
            result=insertNewsList(newsList);
        }
        catch (Exception e)
        {
            result=-1;
            logger.error(e);
        }
        return result;
    }

    /**
     *
     * @return
     */
    @Transactional
    public int insertMovieNews()
    {
        int result=0;
        try
        {
            List<News> newsList=grabMovieNews();
            result=insertNewsList(newsList);
        }
        catch (Exception e)
        {
            result=-1;
            logger.error(e);
        }
        return result;
    }

    /**
     *
     * @return
     */
    @Transactional
    public int insertNBANews()
    {
        int result=0;
        try
        {
            List<News> newsList= grabNBANews();
            result=insertNewsList(newsList);
        }
        catch (Exception e)
        {
            result=-1;
            logger.error(e);
        }
        return result;
    }

    /**
     * 新增新闻列表，如果新闻标题存在，则不新增
     * @param newsList
     * @return
     */
    public int insertNewsList(List<News> newsList)
    {
        int result=0;
        if(null!=newsList && newsList.size()==0){
            return result;
        }
        News n=null;
        for(News news:newsList)
        {
            if(StringUtils.isEmpty(news.getTitle())){
                continue;
            }
            n=new News();
            n.setTitle(news.getTitle());
            if(null==super.select(n))
            {
                result+=super.insert(news);
            }
        }
        return result;
    }

    public List<News> grabITNews() throws IOException
    {
        List<News> newsList=new ArrayList<>();
        String url="http://it.ithome.com/";
        Document document=Jsoup.connect(url).get();
        Elements elements=document.select("div.nlst");
        Elements aTag=elements.select("li a");
        for(int i=0;i<aTag.size();i++)
        {
            String href=aTag.get(i).attr("href");
            newsList.add(analysisITHtml2News(href));
        }
        return newsList;
    }

    /**
     *
     * @return
     * @throws IOException
     */
    public List<News> grabMovieNews() throws IOException
    {
        List<News> newsList=new ArrayList<>();
        String url="http://www.1905.com/film/?fr=homepc_menu_news";
        Document document=Jsoup.connect(url).get();
        Elements elements=document.select("section.mod-top-news");
        Elements aTag=elements.select("h3 a"); // 获取a标签
        for(int i=0;i<aTag.size();i++)
        {
            String href=aTag.get(i).attr("href");
            newsList.add(analysisMovieHtml2News(href));
        }
        return newsList;
    }

    /**
     *
     * @return
     * @throws IOException
     */
    public List<News> grabNBANews() throws IOException
    {
        List<News> newsList=new ArrayList<>();
        String url="http://china.nba.com/";
        Document document=Jsoup.connect(url).get();
        Element modImpNews=document.select("#mod-impnews").first();
        Elements aTag=modImpNews.select("ul.text-news li a"); // 获取a标签
        for(int i=0;i<aTag.size();i++) // 遍历a标签
        {
            String href=aTag.get(i).attr("href");
            newsList.add(analysisNBAHtml2News(href));
        }
        return newsList;
    }

    /**
     * 解析it页面，转为新闻对象
     * @param href
     * @return
     * @throws IOException
     */
    private News analysisITHtml2News(String href) throws IOException
    {
        Document document=Jsoup.connect(href).get();
        Element wrapper=document.select("#wrapper").first();
        String title=wrapper.select("div.post_title h1").html(); // 标题
        Element paragraph=wrapper.select("#paragraph").first();
        Elements imgs=paragraph.select("img");
        for(int i=0;i<imgs.size();i++)
        {
            Element img=imgs.get(i);
            img.attr("src",img.attr("data-original"));
        }
        String content=paragraph.select("p").outerHtml(); // 内容
        News news=new News();
        news.setTitle(title);
        news.setContent(content);
        news.setType(1);
        news.setSource("IT之家");
        news.setLink(href);
        news.setPubdate(new Date());
        news.setHits(0L);
        return news;
    }

    /**
     * 解析movie页面，转换为新闻对象
     * @return
     * @throws IOException
     */
    private News analysisMovieHtml2News(String href) throws IOException
    {
        Document document=Jsoup.connect(href).get();
        Element contentNews=document.select("#contentNews").first();
        String title=contentNews.select("h1.title").html(); // 标题
        String content= StringUtils.replace(contentNews.select("p").outerHtml(), "<br>", ""); // 内容
        News news=new News();
        news.setTitle(title);
        news.setContent(content);
        news.setType(2);
        news.setSource("1905电影网");
        news.setLink(href);
        news.setPubdate(new Date());
        news.setHits(0L);
        return news;
    }

    /**
     * 解析nba页面，转换为新闻对象
     * @param href
     * @return
     * @throws IOException
     */
    private News analysisNBAHtml2News(String href) throws IOException
    {
        Document document=Jsoup.connect(href).get();
        Element article=document.select("#C-Main-Article-QQ").first();
        String title=article.select("div.hd h1").html(); // 标题
        String content=article.select("#Cnt-Main-Article-QQ").first().select("p").outerHtml(); // 内容
        News news=new News();
        news.setTitle(title);
        news.setContent(content);
        news.setType(3);
        news.setSource("NBA中国官方网站");
        news.setLink(href);
        news.setPubdate(new Date());
        news.setHits(0L);
        return news;
    }
}
