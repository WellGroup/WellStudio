package com.well.module.news.mapper;

import com.well.module.news.entity.News;
import tk.mybatis.mapper.common.Mapper;

public interface NewsMapper extends Mapper<News>
{

}