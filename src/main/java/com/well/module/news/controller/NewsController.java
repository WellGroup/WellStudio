package com.well.module.news.controller;

import com.github.pagehelper.PageInfo;
import com.well.module.news.entity.News;
import com.well.module.news.service.NewsService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Copyright &copy; 2017 - 2017 Well All rights reserved.
 * Author:Well
 * Date:2017-06-21
 * Description:新闻控制器类
 */

@Controller
@RequestMapping("/news")
public class NewsController
{
    @Autowired
    private NewsService newsService;

    /**
     * it新闻列表视图
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/itNewsListView")
    public ModelAndView itNewsListView(ModelMap modelMap)
    {
        return new ModelAndView("module/news/newsList",modelMap);
    }

    /**
     * 进入新闻列表页面
     * @param typeName 新闻类型名称
     * @param pageNum 当前页
     * @param pageSize 每一页的数量
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/newsListView/{typeName}")
    public ModelAndView newsListView(@PathVariable String typeName,Integer pageNum,Integer pageSize,ModelMap modelMap)
    {
        int type;
        if(StringUtils.equals(typeName,"it"))
        {
            type=1;
        }
        else if(StringUtils.equals(typeName,"movie"))
        {
            type=2;
        }
        else if(StringUtils.equals(typeName,"nba"))
        {
            type=3;
        }
        else
        {
            type=0;
        }

        pageNum=pageNum==null?1:pageNum;
        pageSize=pageSize==null?10:pageSize;

        PageInfo<News> pageInfo=newsService.selectNewsListByType(type,pageNum,pageSize);
        modelMap.put("typeName",typeName);
        modelMap.put("pageInfo",pageInfo);
        return new ModelAndView("module/news/newsList",modelMap);
    }

}
