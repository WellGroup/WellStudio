package com.well.module.product.controller;

import com.well.module.product.entity.Product;
import com.well.module.product.service.ProductService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Copyright &copy; 2017 - 2017 Well All rights reserved.
 * Author:Well
 * Date:2017-06-23
 * Description:产品控制器
 */

@Controller
@RequestMapping("/product")
public class ProductController
{
    @Autowired
    private ProductService productService;

    /**
     * 产品列表
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "productListView")
    public ModelAndView productListView(ModelMap modelMap)
    {
        Product[][] productss=productService.getProductArray();
        modelMap.put("productss",productss);
        return new ModelAndView("module/product/productList",modelMap);
    }

    /**
     * 产品
     * @param id
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/productView/{id}")
    public ModelAndView productView(@PathVariable String id,ModelMap modelMap)
    {
        if(StringUtils.isBlank(id))
        {
            return new ModelAndView("module/product/productList",modelMap);
        }
        Product product=productService.getProduct(id);
        /* 如果产品为空就进入产品列表页面 */
        if(null==product){
            Product[][] productss=productService.getProductArray();
            modelMap.put("productss",productss);
            return new ModelAndView("module/product/productList",modelMap);
        }

        modelMap.put("product",product);
        return new ModelAndView("module/product/product",modelMap);
    }

}
