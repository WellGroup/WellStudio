package com.well.module.product.mapper;

import com.well.module.product.entity.Product;
import tk.mybatis.mapper.common.Mapper;

public interface ProductMapper extends Mapper<Product>
{
}