package com.well.module.product.service;

import com.github.pagehelper.PageInfo;
import com.well.kernel.service.ABaseService;
import com.well.module.product.entity.Product;
import com.well.module.product.mapper.ProductMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Copyright &copy; 2017 - 2017 Well All rights reserved.
 * Author:Well
 * Date:2017-06-28
 * Description:产品service类
 */

@Service
public class ProductService extends ABaseService<Product>
{
    private static Logger logger=Logger.getLogger(ProductService.class);

    @Autowired
    private ProductMapper productMapper;

    /**
     * 获取最新的产品列表
     * @param rows 产品个数
     * @return
     */
    public List<Product> getLatestProductList(int rows)
    {
        PageInfo<Product> pageInfo=super.selectPageInfo(1,rows);
        return pageInfo.getList();
    }

    /**
     * 获取产品的二维数组，一行显示4个产品
     * @return
     */
    public Product[][] getProductArray()
    {
        List<Product> productList=super.selectList(); // 所有产品列表
        int length=productList.size(); // 产品列表长度
        double rowSize=4.00; // 每行显示4个产品
        int column=(int)rowSize; // 列数
        int row=(int)Math.ceil(length/rowSize); // 行数
        Product[][] productss=new Product[row][column];
        for(int i=0;i<row;i++)
        {
            for(int j=0;j<column;j++)
            {
                productss[i][j]=productList.remove(0);
                if(productList.size()==0)
                {
                    break;
                }
            }
        }
        return productss;
    }

    /**
     * 根据id获取产品信息
     * @param id
     * @return
     */
    public Product getProduct(String id)
    {
        Product product=super.select(id);
        /* 点击量+1 */
        if(null!=product)
        {
            if(null==product.getHits())
            {
                product.setHits(1L);
            }
            else
            {
                product.setHits(product.getHits()+1);
            }
            super.updateSelective(product);
        }
        return product;
    }

}
