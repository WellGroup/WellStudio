package com.well.module.about.service;

import com.well.kernel.service.ABaseService;
import com.well.module.about.entity.Message;
import com.well.module.about.mapper.MessageMapper;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Copyright &copy; 2017 - 2017 Well All rights reserved.
 * Author:Well
 * Date:2017-06-29
 * Description:留言service类
 */

@Service
public class MessageService extends ABaseService<Message>
{
    private static Logger logger=Logger.getLogger(MessageService.class);

    @Autowired
    private MessageMapper messageMapper;

    /**
     * 增加留言
     * @param name
     * @param email
     * @param mobile
     * @param content
     * @param ip
     * @return
     */
    @Transactional
    public int insertMessage(String name,String email,String mobile,String content,String ip)
    {
        int result=0;
        try
        {
            Message message=new Message();
            message.setName(StringEscapeUtils.escapeHtml4(name));
            message.setEmail(StringEscapeUtils.escapeHtml4(email));
            message.setMobile(StringEscapeUtils.escapeHtml4(mobile));
            message.setContent(StringEscapeUtils.escapeHtml4(content));
            message.setIp(ip);
            result = super.insert(message);
        }
        catch (Exception e)
        {
            logger.info(e);
        }
        return result;

    }

}
