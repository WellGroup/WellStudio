package com.well.module.about.mapper;

import com.well.module.about.entity.Message;
import tk.mybatis.mapper.common.Mapper;

public interface MessageMapper extends Mapper<Message>
{
}