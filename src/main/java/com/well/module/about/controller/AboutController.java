package com.well.module.about.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Copyright &copy; 2017 - 2017 Well All rights reserved.
 * Author:Well
 * Date:2017-06-29
 * Description:关于controller
 */

@Controller
@RequestMapping("/about")
public class AboutController
{
    /**
     * 关于页面
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/aboutView")
    public ModelAndView aboutView(ModelMap modelMap)
    {
        return new ModelAndView("module/about/about",modelMap);
    }
}
