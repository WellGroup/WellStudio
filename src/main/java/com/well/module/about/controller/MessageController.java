package com.well.module.about.controller;

import com.well.kernel.spring.web.ResponseData;
import com.well.kernel.util.WebUtil;
import com.well.module.about.entity.Message;
import com.well.module.about.service.MessageService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Copyright &copy; 2017 - 2017 Well All rights reserved.
 * Author:Well
 * Date:2017-06-29
 * Description:留言controller
 */

@Controller
@RequestMapping("/message")
public class MessageController
{
    @Autowired
    private MessageService messageService;

    @RequestMapping(value = "/insertMessage",method = RequestMethod.POST)
    @ResponseBody
    public ResponseData  insertMessage(String name,String email,String mobile,String content,HttpServletRequest request)
    {
        if(StringUtils.isBlank(name))
        {
            return new ResponseData(false,"姓名必须填写！");
        }
        if(name.length()>20 || email.length()>255 || mobile.length()>20 || content.length()>1000)
        {
            return new ResponseData(false,"填写不符合规范！");
        }
        String ip= WebUtil.getIP(request);
        int result=messageService.insertMessage(name,email,mobile,content,ip);
        if(result>0)
        {
            return new ResponseData(true,"留言成功！");
        }
        return new ResponseData(false,"留言失败！");
    }

}
