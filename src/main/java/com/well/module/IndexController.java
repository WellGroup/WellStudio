package com.well.module;

import com.well.kernel.util.WebUtil;
import com.well.module.news.entity.News;
import com.well.module.news.service.NewsService;
import com.well.module.product.entity.Product;
import com.well.module.product.service.ProductService;
import com.well.module.system.service.VisitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 首页控制器
 */

@Controller
public class IndexController
{
    @Autowired
    private NewsService newsService;

    @Autowired
    private ProductService productService;

    @Autowired
    private VisitorService visitorService;

    @RequestMapping(value = "/")
    public ModelAndView index(HttpServletRequest request,ModelMap modelMap)
    {
        String ip= WebUtil.getIP(request); // 获取客户端IP地址
        visitorService.insertByAnalysisIP(ip);
        modelMap.put("ip",ip);

        /* 获取最新6条IT新闻 */
        List<News> itNewsList=newsService.getLatestNewsList(1,6);
        modelMap.put("itNewsList",itNewsList);

        /* 获取最新5条Movie新闻 */
        List<News> movieNewsList=newsService.getLatestNewsList(2, 5);
        modelMap.put("movieNewsList",movieNewsList);

        /* 获取最新5条NBA新闻 */
        List<News> nbaNewsList=newsService.getLatestNewsList(3, 5);
        modelMap.put("nbaNewsList",nbaNewsList);

        /* 获取最新4个产品 */
        List<Product> productList=productService.getLatestProductList(4);
        modelMap.put("productList",productList);

        return new ModelAndView("index",modelMap);
    }
}
