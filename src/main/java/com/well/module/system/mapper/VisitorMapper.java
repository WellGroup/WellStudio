package com.well.module.system.mapper;

import com.well.module.system.entity.Visitor;
import tk.mybatis.mapper.common.Mapper;

public interface VisitorMapper extends Mapper<Visitor>{

}