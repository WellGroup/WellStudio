package com.well.module.system.service;

import com.well.kernel.service.ABaseService;
import com.well.kernel.util.httpclient.HttpAsyncClientUtil;
import com.well.kernel.util.httpclient.HttpClientUtil;
import com.well.kernel.util.httpclient.HttpResponseFutureCallback;
import com.well.module.system.entity.Visitor;
import com.well.module.system.mapper.VisitorMapper;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Copyright &copy; 2017 - 2017 Well All rights reserved.
 * Author:Well
 * Date:2017-06-21
 * Description:
 */

@Service
public class VisitorService extends ABaseService<Visitor>
{
    private static Logger logger=Logger.getLogger(VisitorService.class);

    @Autowired
    private VisitorMapper visitorMapper;

    /**
     * 通过解析ip地址，新增访问者信息
     * @param ip
     * @return
     */
    @Transactional
    public void insertByAnalysisIP(final String ip)
    {
        String uri="http://ip.taobao.com/service/getIpInfo.php?ip="+ip;
        HttpAsyncClientUtil.get(uri,new HttpResponseFutureCallback(){
            @Override
            public void completed(HttpResponse response) {
                try
                {
                    if(null!=response)
                    {
                        String content= EntityUtils.toString(response.getEntity(), "UTF-8");
                        if(StringUtils.isNotEmpty(content))
                        {
                            /*
                                转换为中文内容
                                成功格式：{"code":0,"data":{"country":"中国","country_id":"CN","area":"华东","area_id":"300000","region":"上海市","region_id":"310000","city":"上海市","city_id":"310100","county":"","county_id":"-1","isp":"阿里云","isp_id":"1000323","ip":"106.14.9.22"}}
                                失败格式：{"code":1,"data":""}
                            */
                            String zhContent= StringEscapeUtils.unescapeJava(content);
                            JSONObject jsonObject=JSONObject.fromObject(zhContent);
                            int code=jsonObject.getInt("code");
                            if(code==0){ // 成功
                                JSONObject dataJSONObject=jsonObject.getJSONObject("data"); // data的json对象

                                Visitor visitor=new Visitor();
                                visitor.setIp(ip); // ip地址
                                visitor.setCountry(dataJSONObject.getString("country")); // 国家
                                visitor.setArea(dataJSONObject.getString("area")); // 区域
                                visitor.setRegion(dataJSONObject.getString("region")); // 区域
                                visitor.setCity(dataJSONObject.getString("city")); // 城市
                                visitor.setCounty(dataJSONObject.getString("county")); // 县
                                visitor.setIsp(dataJSONObject.getString("isp")); // 互联网服务提供商

                                insert(visitor);
                            }
                            else
                            {
                                logger.info("无法解析IP地址！");
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.error(e);
                }
            }
        });
    }

}
