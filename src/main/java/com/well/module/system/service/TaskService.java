package com.well.module.system.service;

import com.well.kernel.util.DateUtil;
import com.well.module.news.service.NewsService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;

/**
 * Copyright &copy; 2017 - 2017 Well All rights reserved.
 * Author:Well
 * Date:2017-06-21
 * Description:定时器的service类
 */

@Service
public class TaskService
{
    private static Logger logger=Logger.getLogger(TaskService.class);

    @Autowired
    private NewsService newsService;

    /**
     * 每小时18分18秒执行该任务
     */
    @Scheduled(cron = "18 18 0/1 * * ?")
    @Transactional
    public void insertNews()
    {
        int result=0;

        logger.info(DateUtil.getDateTime()+" 开始抓取IT新闻...");
        result=newsService.insertITNews();
        if(result>0){
            logger.info(DateUtil.getDateTime()+" 抓取【"+result+"】个IT新闻成功！");
        }
        else if(result==0){
            logger.info(DateUtil.getDateTime()+" 没有抓取到新的IT新闻");
        }
        else{
            logger.error(DateUtil.getDateTime()+" 抓取IT新闻失败！");
        }

        logger.info(DateUtil.getDateTime()+" 开始抓取Movie新闻...");
        result=newsService.insertMovieNews();
        if(result>0){
            logger.info(DateUtil.getDateTime()+" 抓取【"+result+"】个Movie新闻成功！");
        }
        else if(result==0){
            logger.info(DateUtil.getDateTime()+" 没有抓取到新的Movie新闻");
        }
        else{
            logger.error(DateUtil.getDateTime()+" 抓取Movie新闻失败！");
        }

        logger.info(DateUtil.getDateTime()+" 开始抓取NBA新闻...");
        result=newsService.insertNBANews();
        if(result>0){
            logger.info(DateUtil.getDateTime()+" 抓取【"+result+"】个NBA新闻成功！");
        }
        else if(result==0){
            logger.info(DateUtil.getDateTime()+" 没有抓取到新的NBA新闻");
        }
        else{
            logger.error(DateUtil.getDateTime()+" 抓取NBA新闻失败！");
        }
    }

}
