<%--
  关于页面
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/view/include/taglib.jsp" %>
<html>
<head>
  <%@ include file="/WEB-INF/view/include/head.jsp" %>
  <title>关于</title>
</head>
<body>

    <%-- 导航栏 --%>
    <jsp:include page="${ctx}/WEB-INF/view/include/navbar.jsp" />

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>在线留言</h1>
                <form id="message_form" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="name" class="col-md-2 control-label">姓名</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="name" name="name" placeholder="王先生" maxlength="10">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-md-2 control-label">邮箱</label>
                        <div class="col-md-8">
                            <input type="email" class="form-control" id="email" name="email" placeholder="18260012083@163.com" maxlength="50">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mobile" class="col-md-2 control-label">手机</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="18260012083" maxlength="20">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                            <label for="content" class="col-md-2 control-label">内容</label>
                            <div class="col-md-8">
                                <textarea class="form-control" id="content" name="content" rows="3" cols="10" placeholder="说点什么吧"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                    <div class="col-md-3">
                        <button type="button" id="message_confirm" class="btn btn-primary">确定</button>
                    </div>
                    <div class="col-md-3">
                        <button type="reset" class="btn btn-default">重置</button>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
                <h1>联系我们</h1>
                <h3>姓名：王先生</h3>
                <h3>手机：18260012083</h3>
                <h3>邮箱：18260012083@163.com</h3>
                <h3>
                    QQ：
                    <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=491808144&site=qq&menu=yes"><img border="0" src="http://wpa.qq.com/pa?p=2:491808144:51" alt="点击这里给我发消息" title="点击这里给我发消息"/></a>
                </h3>
                <h3>
                    简介：All is well! 敬请等待...
                </h3>
            </div>
        </div>
    </div>

    <%-- 页脚 --%>
    <jsp:include page="${ctx}/WEB-INF/view/include/footer.jsp" />

</body>

<script type="text/javascript">

  $(function(){
      $("#message_confirm").click(function(){
          var name=$("#name").val();
          var content=$("#content").val();
          if(null==name || name.length==0){
              bootbox.alert("留个姓名吧！");
              return;
          }
          if(null==content || content.length==0){
              bootbox.alert("说点什么吧！");
              return;
          }

          $.post("${ctx}/message/insertMessage",$("#message_form").serialize(),function(data){
              if(data.success){
                bootbox.alert({
                    message: data.message,
                    callback: function () {
                        /* 清空表单 */
                        $(':input', '#message_form')
                                .not(':button, :submit, :reset, :hidden')
                                .val('')
                                .removeAttr('checked')
                                .removeAttr('selected');
                        }
                    });
              }
              else{
                  bootbox.alert(data.message);
              }
          });
      });
  });

</script>

</html>

