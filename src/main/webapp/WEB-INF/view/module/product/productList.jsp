<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  产品列表页面
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/view/include/head.jsp" %>
    <title>产品列表</title>
</head>
<body>
    <%-- 导航栏 --%>
    <jsp:include page="${ctx}/WEB-INF/view/include/navbar.jsp" />

    <div class="container">
        <c:forEach var="products" items="${productss}" varStatus="status">
            <div class="row">
                <c:forEach var="product" items="${products}" varStatus="s">
                    <c:if test="${! empty product}">
                        <div class="col-md-3">
                            <a href="${ctx}/product/productView/${product.id}" class="thumbnail">
                                <img src="${ctx}/${product.thumbnail}" alt="">
                                <div class="caption">
                                    <h3>${product.name}</h3>
                                    <p>${product.summary}</p>
                                </div>
                            </a>
                        </div>
                    </c:if>
                </c:forEach>
            </div>
        </c:forEach>
    </div>

    <%-- 页脚 --%>
    <jsp:include page="${ctx}/WEB-INF/view/include/footer.jsp" />

</body>
</html>
