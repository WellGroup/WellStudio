<%--
  产品页面
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/view/include/taglib.jsp" %>
<html>
<head>
  <%@ include file="/WEB-INF/view/include/head.jsp" %>
  <title>产品</title>
</head>
<body>

    <%-- 导航栏 --%>
    <jsp:include page="${ctx}/WEB-INF/view/include/navbar.jsp" />

    <div class="container">
        <h1>${product.name} <small>${product.summary}</small></h1>
        <h6>
            <small class="product_updateDate">
                时间：<fmt:formatDate value="${product.pubdate}" pattern="yyyy-MM-dd HH:mm:ss" />
            </small>

            <small class="product_hits">
                点击量：${product.hits}
            </small>
        </h6>
        <div class="row">
            <div class="col-md-8">
                <div class="product_thumbnail text-center">
                    <img src="${ctx}/${product.thumbnail}" width="500" height="300" alt="${product.name}" />
                </div>
                <div class="product_content text-left">
                    <p>
                        文档还在整理中，敬请期待......
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <a href="javascript:void(0);" class="thumbnail">
                    <img src="${ctx}/static/image/alliswell.jpg" alt="alliswell">
                    <div class="caption">
                        <p>不知道为什么，就是在这放张图片</p>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <%-- 页脚 --%>
    <jsp:include page="${ctx}/WEB-INF/view/include/footer.jsp" />

    <!-- 模态框（Modal），显示大图 -->
    <div class="modal fade text-center" id="large_img_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="display: inline-block; width: auto;">
            <div class="modal-content" id="large_img_src">
            </div>
        </div>
    </div>

</body>

<script type="text/javascript">

    $(function(){
        if(isPC()){
            /* 所有的图片注册点击事件 */
            $("img").click(function(){
                showLargeImgModal(this);
            });
        }
    });

    /* 显示大图模态框 */
    function showLargeImgModal(ths){
        var src=ths.src;
        var alt=ths.alt;
        $("#large_img_src").html("<img src='"+src+"' alt='"+alt+"' />");
        $("#large_img_modal").modal("show");
    }

    /* 判断是否为电脑端访问 */
    function isPC() {
        var sUserAgent = navigator.userAgent.toLowerCase();
        var bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
        var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
        var bIsMidp = sUserAgent.match(/midp/i) == "midp";
        var bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
        var bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";
        var bIsAndroid = sUserAgent.match(/android/i) == "android";
        var bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";
        var bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";
        if (bIsIpad || bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM) {
            return false;
        } else {
            return true;
        }
    }

</script>

</html>
