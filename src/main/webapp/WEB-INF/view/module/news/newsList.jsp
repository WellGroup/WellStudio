<%--
  新闻列表页面
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/view/include/taglib.jsp" %>
<html>
<head>
  <%@ include file="/WEB-INF/view/include/head.jsp" %>
  <title>新闻列表</title>
</head>
<body>

    <%-- 导航栏 --%>
    <jsp:include page="${ctx}/WEB-INF/view/include/navbar.jsp" />

    <div class="container">
        <%-- 第一列面板 --%>
        <div class="panel panel-info">
            <div class="panel-heading">${typeName}</div>
            <c:forEach var="news" items="${pageInfo.list}" varStatus="status">
                <a href="${news.link}" target="_blank" class="list-group-item" title="${news.title}">
                            <span class="badge">
                                <fmt:formatDate value="${news.updateDate}" pattern="yyyy-MM-dd"/>
                            </span>
                        ${news.title}
                </a>
            </c:forEach>
        </div>

        <ul id="pager" class="pager">
            <li name="first_page"><a href="${ctx}/news/newsListView/${typeName}">首页</a></li>
            <li name="pre_page"><a href="${ctx}/news/newsListView/${typeName}?pageNum=${pageInfo.prePage}">上一页</a></li>
            <li class="disabled"><a href="javascript:void(0);">${pageInfo.pageNum}/${pageInfo.pages}</a></li>
            <li name="next_page"><a href="${ctx}/news/newsListView/${typeName}?pageNum=${pageInfo.nextPage}">下一页</a></li>
            <li name="last_page"><a href="${ctx}/news/newsListView/${typeName}?pageNum=${pageInfo.pages}">末页</a></li>
        </ul>
    </div>

    <%-- 页脚 --%>
    <jsp:include page="${ctx}/WEB-INF/view/include/footer.jsp" />

</body>

<script type="text/javascript">

    $(function(){
        var isFirstPage=${pageInfo.isFirstPage};
        if(isFirstPage){
            var $first_page=$("#pager li[name=first_page]");
            $first_page.addClass("disabled");
            $first_page.children("a").attr("href","javascript:void(0);");

            var $pre_page=$("#pager li[name=pre_page]");
            $pre_page.addClass("disabled");
            $pre_page.children("a").attr("href","javascript:void(0);");
        }
        var isLastPage=${pageInfo.isLastPage};
        if(isLastPage){
            var $last_page=$("#pager li[name=last_page]");
            $last_page.addClass("disabled");
            $last_page.children("a").attr("href","javascript:void(0);");

            var $next_page=$("#pager li[name=next_page]");
            $next_page.addClass("disabled");
            $next_page.children("a").attr("href","javascript:void(0);");
        }
    });

</script>

</html>
