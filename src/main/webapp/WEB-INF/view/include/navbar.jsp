<%--
  导航栏页面
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<nav class="navbar navbar-default" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <div class="navbar-brand">
        <p>Well工作室</p>
      </div>
    </div>
    <div>
      <ul class="nav navbar-nav navbar-left">
        <li><a href="/">首页</a></li>
        <li class="dropdown">
            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                新闻
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <li><a href="${ctx}/news/newsListView/it">IT新闻</a></li>
                <li class="divider"></li>
                <li><a href="${ctx}/news/newsListView/movie">Movie新闻</a></li>
                <li class="divider"></li>
                <li><a href="${ctx}/news/newsListView/nba">NBA新闻</a></li>
            </ul>
        </li>
        <li><a href="${ctx}/product/productListView">产品</a></li>
        <li><a href="${ctx}/about/aboutView">关于</a></li>
      </ul>

      <%--<form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="请输入关键字">
        </div>
        <button type="submit" class="btn btn-default">搜索</button>
      </form>--%>

      <%--<ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> 注册</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> 登录</a></li>
      </ul>
      <p class="navbar-text navbar-right">您好，游客！</p>--%>
    </div>
  </div>
</nav>
