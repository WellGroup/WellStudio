<%--
  页面头部，添加在标签<head></head>之间
  <jsp:include page="include/head.jsp" />
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->

<!-- 浏览器地址栏的ico -->
<link rel="icon" type="image/x-icon" href="${ctx}/static/image/ico/well.ico" />
<link rel="shortcut icon" type="image/x-icon" href="${ctx}/static/image/ico/well.ico" />
<link rel="bookmark" type="image/x-icon" href="${ctx}/static/image/ico/well.ico" />

<!-- Bootstrap -->
<link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<style type="text/css">
    a{text-decoration: none;}
    a:link{text-decoration:none;}   /* 指正常的未被访问过的链接*/
    a:visited{text-decoration:none;} /*指已经访问过的链接*/
    a:hover{text-decoration:none;} /*指鼠标在链接*/
    a:active{text-decoration:none;} /* 指正在点的链接*/

    .height10{height: 10px;}
    .height20{height: 20px;}
    .padding-top10{padding: 10px 0 0 0;}
    .margin-top10{margin: 10px 0 0 0;}
    .product_updateDate{padding: 0 5px 0 0;}
    .product_hits{padding: 0 0 0 5px;}
    .product_thumbnail{padding: 10px 0 20px 0;}
    .product_content{}
</style>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Bootstrap modals create programmatic dialog boxes  -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>

