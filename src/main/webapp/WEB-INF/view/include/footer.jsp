<%--
  页脚
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="page-header"></div>

<div class="container-fluid">
  <%-- 网格，一列 --%>
  <div class="row">
    <div class="col-md-12 text-center" style="background-color: rgba(0, 0, 0, 0.8);color: #CFCFCF;height: 30px;line-height: 30px;">
      版权所有 &copy; Well工作室
    </div>
  </div>
</div>
