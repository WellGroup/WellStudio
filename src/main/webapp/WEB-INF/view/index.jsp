<%-- 首页 --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/view/include/taglib.jsp" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <%@ include file="/WEB-INF/view/include/head.jsp" %>
    <title>Well工作室</title>
</head>
<body>
    <%-- 导航栏 --%>
    <jsp:include page="${ctx}/WEB-INF/view/include/navbar.jsp" />

    <div class="container">
        <div class="alert alert-warning  fade in">
            <button class="close" data-dismiss="alert"><span>&times;</span></button>
            <p>网站正在开发中...</p>
        </div>

        <%-- 轮播图片 --%>
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- 轮播（Carousel）指标 -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>
            <!-- 轮播（Carousel）项目 -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <a href="">
                        <img src="${ctx}/static/image/1.jpg" width="100%" alt="1">
                        <div class="carousel-caption">
                        </div>
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <img src="${ctx}/static/image/2.jpg" width="100%" alt="2">
                        <div class="carousel-caption">
                        </div>
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <img src="${ctx}/static/image/3.jpg" width="100%" alt="3">
                        <div class="carousel-caption">
                        </div>
                    </a>
                </div>
            </div>
            <!-- 轮播（Carousel）导航 -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">上一张</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">下一张</span>
            </a>
        </div>

        <div class="height20"></div>

        <%-- 网格，两列 --%>
        <div class="row">
            <div class="col-md-9">
                <%-- 第一列面板 --%>
                <div class="panel panel-info">
                    <div class="panel-heading">IT</div>
                    <c:forEach var="itNews" items="${itNewsList}" varStatus="status">
                        <a href="${itNews.link}" target="_blank" class="list-group-item" title="${itNews.title}">
                            <span class="badge">
                                <fmt:formatDate value="${itNews.updateDate}" pattern="yyyy-MM-dd"/>
                            </span>
                                ${itNews.title}
                        </a>
                    </c:forEach>
                </div>
            </div>
            <div class="col-md-3">
                <a href="javascript:void(0);" class="thumbnail">
                    <img src="${ctx}/static/image/alliswell.jpg" alt="alliswell">
                    <div class="caption">
                        <h3>All is Well!</h3>
                        <p>不知道为什么，就是在这放张图片</p>
                    </div>
                </a>
            </div>
        </div>

        <%-- 网格，两列 --%>
        <div class="row">
            <div class="col-md-6">
                <%-- 第一列面板 --%>
                <div class="panel panel-info">
                    <div class="panel-heading">Movie</div>
                    <c:forEach var="movieNews" items="${movieNewsList}" varStatus="status">
                        <a href="${movieNews.link}" target="_blank" class="list-group-item" title="${movieNews.title}">
                            <span class="badge">
                                <fmt:formatDate value="${movieNews.updateDate}" pattern="yyyy-MM-dd"/>
                            </span>
                                ${movieNews.title}
                        </a>
                    </c:forEach>
                </div>
            </div>
            <div class="col-md-6">
                <%-- 第二列面板 --%>
                <div class="panel panel-info">
                    <div class="panel-heading">NBA</div>
                    <c:forEach var="nbaNews" items="${nbaNewsList}" varStatus="status">
                        <a href="${nbaNews.link}" target="_blank" class="list-group-item" title="${nbaNews.title}">
                            <span class="badge">
                                <fmt:formatDate value="${nbaNews.updateDate}" pattern="yyyy-MM-dd"/>
                            </span>
                            ${nbaNews.title}
                        </a>
                    </c:forEach>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">Product</div>
                    <div class="row margin-top10">
                        <c:forEach var="product" items="${productList}" varStatus="status">
                            <div class="col-md-3">
                                <a href="${ctx}/product/productView/${product.id}" class="thumbnail">
                                    <img src="${ctx}/${product.thumbnail}">
                                    <div class="caption">
                                        <h3>${product.name}</h3>
                                        <p>${product.summary}</p>
                                    </div>
                                </a>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <%-- 页脚 --%>
    <jsp:include page="${ctx}/WEB-INF/view/include/footer.jsp" />

</body>

<script type="text/javascript">
    $(function(){

    });
</script>

</html>